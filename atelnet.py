# import getpass
# import sys
import telnetlib
import ConfigParser
import time
import traceback

config = ConfigParser.RawConfigParser()
config.read('telnet.ini')

for s in config.sections():
    print (config.get(s,'host'))

    try:
        tn = telnetlib.Telnet(config.get(s,'host'))
        # tn.set_debuglevel(9)
        for x in range(1,11):
            # print(x)
            if config.has_option(s,'cmd%s' % x):
                time.sleep(1)
                tn.read_until('>',15)
                cmd = "%s\r" % config.get(s,'cmd%s' % x)
                cmd = cmd.replace('<datetime>',time.strftime("%Y-%m-%d %H:%M:%S"))
                cmd = cmd.replace('<date>',time.strftime("%Y-%m-%d"))
                cmd = cmd.replace('<time>',time.strftime("%H:%M:%S"))
                print(cmd)
                tn.write(cmd)
                # print tn.read_some()

        time.sleep(1)
        tn.read_until('>',15)
        print(config.get(s,'cmdexit'))
        tn.write("%s\r" % config.get(s,'cmdexit'))
        print (tn.read_all())
        tn.close()
    except Exception as e:
        # print(e)
        traceback.print_exc()
